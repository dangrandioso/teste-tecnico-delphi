object Dados: TDados
  OnCreate = DataModuleCreate
  Height = 750
  Width = 1000
  PixelsPerInch = 120
  object sqlcon_LancamentoPedidos: TSQLConnection
    ConnectionName = 'MySQLConnection'
    DriverName = 'MySQL'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXMySQL'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver280.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXMySqlMetaDataCommandFactory,DbxMySQLDr' +
        'iver280.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXMySqlMetaDataCommandFact' +
        'ory,Borland.Data.DbxMySQLDriver,Version=24.0.0.0,Culture=neutral' +
        ',PublicKeyToken=91d62ebb5b0d1b1b'
      'GetDriverFunc=getSQLDriverMYSQL'
      'LibraryName=dbxmys.dll'
      'LibraryNameOsx=libsqlmys.dylib'
      'VendorLib=LIBMYSQL.dll'
      'VendorLibWin64=libmysql.dll'
      'VendorLibOsx=libmysqlclient.dylib'
      'MaxBlobSize=-1'
      'DriverName=MySQL'
      'HostName=Localhost'
      'Database=lancamentopedidos'
      'User_Name=root'
      'Password=SimRagOdiShu4@'
      'ServerCharSet='
      'BlobSize=-1'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'Compressed=False'
      'Encrypted=False'
      'ConnectTimeout=60'
      'ConnectionName=MySQLConnection')
    Connected = True
    Left = 120
    Top = 32
  end
  object sqlds__BuscaClientes: TSQLDataSet
    CommandText = 'select `CodigoCliente`,`NomeCliente` from `clientes`'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = sqlcon_LancamentoPedidos
    Left = 88
    Top = 360
  end
  object dts_BuscaClientes: TDataSource
    DataSet = sqlds__BuscaClientes
    Left = 96
    Top = 264
  end
  object sqlds_BuscaProdutos: TSQLDataSet
    CommandText = 'select `CodigoProduto`, `Descricao`  from `produtos`'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = sqlcon_LancamentoPedidos
    Left = 304
    Top = 336
  end
  object dts_BuscaProdutos: TDataSource
    DataSet = sqlds__BuscaClientes
    Left = 288
    Top = 416
  end
  object SQLQUERY: TSQLQuery
    MaxBlobSize = -1
    Params = <>
    SQLConnection = sqlcon_LancamentoPedidos
    Left = 112
    Top = 144
  end
  object sql_SP: TSQLStoredProc
    MaxBlobSize = -1
    Params = <>
    SQLConnection = sqlcon_LancamentoPedidos
    Left = 592
    Top = 544
  end
  object SQLMonitor: TSQLMonitor
    SQLConnection = sqlcon_LancamentoPedidos
    Left = 672
    Top = 552
  end
end
