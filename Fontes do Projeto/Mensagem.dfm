object frm_Mensagem: Tfrm_Mensagem
  AlignWithMargins = True
  Left = 0
  Top = 0
  AutoSize = True
  ClientHeight = 189
  ClientWidth = 560
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poMainFormCenter
  TextHeight = 15
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 554
    Height = 183
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 0
    ExplicitTop = 95
    ExplicitWidth = 22
    ExplicitHeight = 170
    object lbl_Mensagem: TLabel
      AlignWithMargins = True
      Left = 75
      Top = 38
      Width = 546
      Height = 50
      Align = alCustom
      Anchors = [akLeft, akRight]
      AutoSize = False
      ExplicitLeft = 0
      ExplicitTop = 37
      ExplicitWidth = 552
    end
    object btn_Verdadeiro: TButton
      AlignWithMargins = True
      Left = 134
      Top = 91
      Width = 100
      Height = 56
      Align = alCustom
      Anchors = [akTop, akBottom]
      Caption = 'OK'
      DisabledImageName = 'btn_Click1'
      TabOrder = 0
      Visible = False
      OnClick = btn_VerdadeiroClick
      ExplicitLeft = 127
      ExplicitTop = 86
    end
    object btn_Falso: TButton
      AlignWithMargins = True
      Left = 337
      Top = 88
      Width = 100
      Height = 56
      Align = alCustom
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      DisabledImageName = 'btn_falso'
      TabOrder = 1
      Visible = False
      OnClick = btn_FalsoClick
      ExplicitLeft = 332
      ExplicitTop = 83
    end
  end
end
