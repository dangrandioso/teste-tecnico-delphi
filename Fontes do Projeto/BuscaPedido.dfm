object frm_BuscarPedido: Tfrm_BuscarPedido
  Left = 0
  Top = 0
  Caption = 'Digite o Pedido'
  ClientHeight = 149
  ClientWidth = 355
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poMainFormCenter
  TextHeight = 15
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 355
    Height = 442
    Align = alTop
    TabOrder = 3
    ExplicitLeft = 80
    ExplicitTop = 184
    ExplicitWidth = 185
    ExplicitHeight = 41
  end
  object btn_retorna: TButton
    Left = 248
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Retorna'
    TabOrder = 0
    OnClick = btn_retornaClick
  end
  object btn_confirma: TButton
    Left = 32
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Confirma'
    Enabled = False
    TabOrder = 1
    OnClick = btn_confirmaClick
  end
  object edt_NumPedido: TEdit
    Left = 32
    Top = 32
    Width = 305
    Height = 23
    NumbersOnly = True
    TabOrder = 2
    OnChange = edt_NumPedidoChange
  end
end
