program Pedidos_Teste;

uses
  Vcl.Forms,
  LancamentoPedido in 'LancamentoPedido.pas' {frm_LancamentoPedido},
  DBCONECTA in 'DBCONECTA.pas' {Dados: TDataModule},
  BuscaPedido in 'BuscaPedido.pas' {frm_BuscarPedido},
  Mensagem in 'Mensagem.pas' {frm_Mensagem};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tfrm_LancamentoPedido, frm_LancamentoPedido);
  Application.CreateForm(TDados, Dados);
  Application.CreateForm(Tfrm_BuscarPedido, frm_BuscarPedido);
  Application.CreateForm(Tfrm_Mensagem, frm_Mensagem);
  Application.Run;
end.
