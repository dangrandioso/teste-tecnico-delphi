unit DBCONECTA;

interface

uses
  System.SysUtils, System.Classes, Data.DBXMySQL, Data.DB, Data.SqlExpr,
  Data.FMTBcd, Datasnap.DBClient, Datasnap.Provider, IniFiles,IBX.IBDatabaseINI;

type
  TDados = class(TDataModule)
    sqlcon_LancamentoPedidos: TSQLConnection;
    sqlds__BuscaClientes: TSQLDataSet;
    dts_BuscaClientes: TDataSource;
    sqlds_BuscaProdutos: TSQLDataSet;
    dts_BuscaProdutos: TDataSource;
    SQLQUERY: TSQLQuery;
    sql_SP: TSQLStoredProc;
    SQLMonitor: TSQLMonitor;
    Procedure CancelaPedido(CodPedido:Integer);
    Procedure GravaPedido(CodCliente:Integer;ValorTotal: Double );
    Procedure GravaPedidoProduto(CodPedido,CodProduto:Integer;Quantidade,ValorUnitario,ValorTotal:Double);
    Procedure criatansacao;
    Procedure fechatransacao;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Sucesso : Boolean;
    TD: TTransactionDesc  ;
  end;

var
  Dados: TDados;

implementation
 uses LancamentoPedido ;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}



procedure TDados.GravaPedido(CodCliente:Integer;ValorTotal: Double);
begin
   Sucesso := false;
   LancamentoPedido.frm_LancamentoPedido.CodPedido := 0;
   criatansacao;
   Try
        sql_SP.StoredProcName := 'GRAVAR_PEDIDO';
        sql_SP.ParamByName('PAR_CODCLIENTE').AsInteger  := CodCliente;
        sql_SP.ParamByName('PAR_VALORTOTAL').AsFloat    := ValorTotal;
        sql_SP.Prepared := True;
        sql_SP.ExecProc;
        if sql_SP.Params.FindParam('PAR_NUMEROPEDIDO') <> nil then
        Begin
            LancamentoPedido.frm_LancamentoPedido.CodPedido := sql_SP.Params.FindParam('PAR_NUMEROPEDIDO').AsInteger;
            Sucesso := True;
        End;
   except
        Sucesso := false;
   End;
        fechatransacao;
end;

procedure TDados.GravaPedidoProduto(CodPedido,CodProduto:Integer;Quantidade,ValorUnitario,ValorTotal:Double);
begin
   Sucesso := false;
    criatansacao;
   Try
        sql_SP.StoredProcName := 'GRAVAR_PEDIDO_PRODUTO';
        sql_SP.ParamByName('PAR_NUMEROPEDIDO').AsInteger   := CodPedido;
        sql_SP.ParamByName('PAR_CODIGOPRODUTO').AsInteger   := CodProduto;
        sql_SP.ParamByName('PAR_QUANTIDADE').AsFloat    := Quantidade;
        sql_SP.ParamByName('PAR_VALORUNITARIO').AsFloat    := ValorUnitario;
        sql_SP.ParamByName('PAR_VALORTOTAL').AsFloat    := ValorTotal;
        sql_SP.Prepared := True;
        sql_SP.ExecProc;
        Sucesso := True;
   except
        Sucesso := false;
   End;
        fechatransacao;
end;

procedure TDados.CancelaPedido(CodPedido:Integer);
begin
   Sucesso := false;
   criatansacao;
   Try
        sql_SP.StoredProcName := 'DELETAR_PEDIDO';
        sql_SP.ParamByName('PAR_NUMEROPEDIDO').AsInteger    := CodPedido;
        sql_SP.Prepared := True;
        sql_SP.ExecProc;
        Sucesso := True;
   except
        Sucesso := false;
   End;
        fechatransacao;
end;

procedure TDados.criatansacao;
begin
   if not sqlcon_LancamentoPedidos.InTransaction then
   begin
     TD.TransactionID := 1;
     TD.IsolationLevel := xilREADCOMMITTED;
     sqlcon_LancamentoPedidos.StartTransaction(TD);
   end;
end;

procedure TDados.DataModuleCreate(Sender: TObject);
Var
 R:TIniFile;
 databasename,drivername,user,senha,hostname:String;

begin
   R:=TIniFile.Create(extractfilepath(ParamStr(0))+'conexao.ini');{loja.ini � o nome do arquivo criado na pasta de seu programa}
   databasename:=R.ReadString('Database','Database','');
   drivername:=R.ReadString('DriverName','DriverName','');
   hostname:=R.ReadString('HostName','HostName','');
   user:=R.ReadString('User_Name','User_Name','');
   senha:=R.ReadString('Password','Password','');


  R:=TIniFile.Create(extractfilepath(ParamStr(0))+'conexao.ini');{loja.ini � o nome do arquivo criado na pasta de seu programa}
  databasename:=R.ReadString('Database','Database','');
  FreeAndNil(R);
  

end;

procedure TDados.fechatransacao;
begin

   if (sqlcon_LancamentoPedidos.InTransaction) then
   begin
      sqlcon_LancamentoPedidos.Commit(TD);
   end;
end;
end.
