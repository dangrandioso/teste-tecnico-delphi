object frm_LancamentoPedido: Tfrm_LancamentoPedido
  Left = 0
  Top = 0
  Align = alCustom
  AutoSize = True
  Caption = 'Lan'#231'amento de Pedido'
  ClientHeight = 691
  ClientWidth = 1054
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poDesktopCenter
  OnShow = FormShow
  TextHeight = 15
  object pnl_Cabecalho: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 1048
    Height = 70
    Align = alTop
    TabOrder = 1
    object lb_Cliente: TLabel
      Left = 17
      Top = 20
      Width = 85
      Height = 14
      Caption = 'Codigo Cliente: '
    end
    object edt_CodCliente: TEdit
      Left = 108
      Top = 17
      Width = 213
      Height = 23
      Enabled = False
      TabOrder = 0
    end
    object btn_BuscarPedido: TButton
      Left = 849
      Top = 16
      Width = 121
      Height = 25
      Caption = 'Buscar Pedido'
      TabOrder = 1
      OnClick = btn_BuscarPedidoClick
    end
    object cboCliente: TComboBox
      Left = 360
      Top = 17
      Width = 313
      Height = 23
      TabOrder = 2
      OnChange = cboClienteChange
      OnEnter = cboClienteEnter
    end
  end
  object pnl_grid: TPanel
    AlignWithMargins = True
    Left = 416
    Top = 79
    Width = 635
    Height = 507
    Align = alRight
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object dbg_Pedido: TDBGrid
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 627
      Height = 499
      Align = alClient
      DataSource = dts_Pedidos
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'Segoe UI'
      TitleFont.Style = []
      OnKeyDown = dbg_PedidoKeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'codProd'
          Title.Caption = 'Codigo Produto'
          Width = 89
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 248
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'qtde'
          Title.Caption = 'Quantidade'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VlrUnit'
          Title.Caption = 'Valor Unit'#225'rio'
          Width = 99
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VlrTotal'
          Title.Caption = 'Valor Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'codpeditem'
          Visible = False
        end>
    end
  end
  object pnlRodape: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 592
    Width = 1048
    Height = 96
    Align = alBottom
    TabOrder = 2
    ExplicitLeft = -13
    ExplicitTop = 568
    object btn_CancelarPedido: TButton
      Left = 784
      Top = 56
      Width = 121
      Height = 25
      Caption = 'Cancelar Pedido'
      TabOrder = 0
      OnClick = btn_CancelarPedidoClick
    end
    object btn_GravarPedido: TButton
      Left = 924
      Top = 56
      Width = 94
      Height = 25
      Caption = 'Gravar Pedido'
      TabOrder = 1
      OnClick = btn_GravarPedidoClick
    end
    object edt_TotalPedido: TEdit
      Left = 849
      Top = 19
      Width = 149
      Height = 23
      Enabled = False
      NumbersOnly = True
      TabOrder = 2
      TextHint = 'Valor Total'
    end
  end
  object pnl_produtos: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 79
    Width = 407
    Height = 507
    Align = alLeft
    Anchors = [akLeft, akTop, akRight, akBottom]
    Enabled = False
    TabOrder = 3
    object lb_Produto: TLabel
      Left = 17
      Top = 45
      Width = 91
      Height = 15
      Caption = 'Codigo Produto: '
    end
    object lb_Quantidade: TLabel
      Left = 17
      Top = 114
      Width = 68
      Height = 15
      Caption = 'Quantidade: '
    end
    object lb_Valor: TLabel
      Left = 17
      Top = 181
      Width = 77
      Height = 15
      Caption = 'Valor Unit'#225'rio: '
    end
    object edt_Quantidade: TEdit
      Left = 172
      Top = 106
      Width = 149
      Height = 23
      NumbersOnly = True
      TabOrder = 0
      TextHint = 'Quantidade do Produto'
    end
    object edt_ValorUnitario: TEdit
      Left = 172
      Top = 178
      Width = 149
      Height = 23
      NumbersOnly = True
      TabOrder = 1
      TextHint = 'Valor Unitario'
    end
    object btn_AdicionarProduto: TButton
      Left = 204
      Top = 256
      Width = 117
      Height = 25
      Caption = 'Adicionar Produto'
      Enabled = False
      TabOrder = 2
      OnClick = btn_AdicionarProdutoClick
    end
    object cboProduto: TComboBox
      Left = 17
      Top = 66
      Width = 304
      Height = 23
      TabOrder = 3
      OnChange = cboProdutoChange
    end
  end
  object cds_Pedidos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodProd'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'descricao'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'qtde'
        DataType = ftFloat
      end
      item
        Name = 'VlrUnit'
        Attributes = [faRequired]
        DataType = ftFloat
      end
      item
        Name = 'VlrTotal'
        DataType = ftFloat
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dsp_Pedidos'
    StoreDefs = True
    Left = 56
    Top = 608
  end
  object dts_Pedidos: TDataSource
    DataSet = cds_Pedidos
    Left = 128
    Top = 608
  end
end
