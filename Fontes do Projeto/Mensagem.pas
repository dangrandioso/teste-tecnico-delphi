unit Mensagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  Tfrm_Mensagem = class(TForm)
    btn_Verdadeiro: TButton;
    lbl_Mensagem: TLabel;
    Panel1: TPanel;
    btn_Falso: TButton;
    procedure btn_VerdadeiroClick(Sender: TObject);
    procedure btn_FalsoClick(Sender: TObject);
  private
    { Private declarations }
  public
    RetornoMensagem : Boolean ;
  end;

var
  frm_Mensagem: Tfrm_Mensagem;

implementation

{$R *.dfm}

procedure Tfrm_Mensagem.btn_VerdadeiroClick(Sender: TObject);
begin
  RetornoMensagem  := True;
  Close;
end;

procedure Tfrm_Mensagem.btn_FalsoClick(Sender: TObject);
begin
  RetornoMensagem  := False;
  Close;
end;
end.
