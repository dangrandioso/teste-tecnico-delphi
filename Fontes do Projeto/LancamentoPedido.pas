unit LancamentoPedido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.DBCtrls,
  Vcl.Grids, Vcl.DBGrids, Data.FMTBcd, Data.SqlExpr, Datasnap.DBClient, SimpleDS,
  Data.DBXMySQL, Vcl.Mask, Vcl.ExtCtrls, DBCONECTA, Datasnap.Provider,BuscaPedido;

type
  Tfrm_LancamentoPedido = class(TForm)
    dbg_Pedido: TDBGrid;
    lb_Produto: TLabel;
    lb_Valor: TLabel;
    lb_Cliente: TLabel;
    lb_Quantidade: TLabel;
    btn_AdicionarProduto: TButton;
    btn_BuscarPedido: TButton;
    edt_Quantidade: TEdit;
    edt_ValorUnitario: TEdit;
    btn_CancelarPedido: TButton;
    btn_GravarPedido: TButton;
    edt_TotalPedido: TEdit;
    cboCliente: TComboBox;
    cboProduto: TComboBox;
    cds_Pedidos: TClientDataSet;
    dts_Pedidos: TDataSource;
    pnl_grid: TPanel;
    pnl_Cabecalho: TPanel;
    pnlRodape: TPanel;
    pnl_produtos: TPanel;
    edt_CodCliente: TEdit;
    procedure LimpaDados;
    procedure PreencheProduto;
    procedure PreencheCliente;
    procedure PreenchePedido;
    procedure btn_BuscarPedidoClick(Sender: TObject);
    procedure btn_CancelarPedidoClick(Sender: TObject);
    procedure btn_GravarPedidoClick(Sender: TObject);
    procedure btn_AdicionarProdutoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cboProdutoChange(Sender: TObject);
    procedure cboClienteChange(Sender: TObject);
    procedure dbg_PedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cboClienteEnter(Sender: TObject);
  private
    { Private declarations }
    CodCliente    : Integer;
    CodProduto    : Integer;
    DescProduto   : String;
    GravaPedido   : Boolean;
    DeletaPedido  : Boolean;
    Alteracao     : Boolean;

  public
    { Public declarations }
    CodPedido    : Integer;
  end;

var
  frm_LancamentoPedido: Tfrm_LancamentoPedido;

implementation

{$R *.dfm}

uses Mensagem;



procedure Tfrm_LancamentoPedido.PreencheProduto;
begin
   CodProduto := 0;
   cboProduto.Items.Clear;
   cboProduto.Items.Add('');
   Dados.SQLQUERY.Close();
   Dados.SQLQUERY.SQL.Clear;
   Dados.SQLQUERY.SQL.Add('select CodigoProduto,Descricao ');
   Dados.SQLQUERY.SQL.add('from Produtos');
   Dados.SQLQUERY.SQL.add('ORDER BY Descricao');
   Dados.SQLQUERY.Open;
  while not Dados.SQLQUERY.Eof do begin
    cboProduto.Items.Add((Dados.SQLQUERY.FieldByName('CodigoProduto').AsString) + ' - ' + Trim(Dados.SQLQUERY.FieldByName('Descricao').AsString));
    Dados.SQLQUERY.Next;
  end;
   Dados.SQLQUERY.Close;
end;

procedure Tfrm_LancamentoPedido.PreencheCliente;
begin
   CodCliente := 0;
   cboCliente.Items.Clear;
   cboCliente.Items.Add('');
   Dados.SQLQUERY.Close();
   Dados.SQLQUERY.SQL.Clear;
   Dados.SQLQUERY.SQL.Add('select CodigoCliente,NomeCliente ');
   Dados.SQLQUERY.SQL.add('from Clientes');
   Dados.SQLQUERY.SQL.add('ORDER BY NomeCliente');
   Dados.SQLQUERY.Open;
  while not Dados.SQLQUERY.Eof do begin
    cboCliente.Items.Add((Dados.SQLQUERY.FieldByName('CodigoCliente').AsString) + ' - ' + Trim(Dados.SQLQUERY.FieldByName('NomeCliente').AsString));
    Dados.SQLQUERY.Next;
  end;
   Dados.SQLQUERY.Close;
end;


procedure Tfrm_LancamentoPedido.btn_AdicionarProdutoClick(Sender: TObject);
Var ValorUnitario , Quantidade , ValorTotal , TotalGeral : Double;
    mensagem : string;
begin
     mensagem := '';
     TotalGeral    :=  0;
     if edt_ValorUnitario.text = '' then
     begin
        mensagem := 'Valor Unitario N�o pode ser Zerado!' ;
     end;
     if edt_Quantidade.text = '' then
     begin
        mensagem := mensagem +  #13 +' Quantidade N�o pode ser Zerada!' ;
     end;
     if mensagem <> '' then
     Begin
         Application.CreateForm( tfrm_Mensagem, frm_Mensagem );
         frm_Mensagem.Caption := 'ERRO!';
         frm_Mensagem.lbl_Mensagem.Caption := mensagem; ;
         frm_Mensagem.btn_Falso.Caption := 'OK';
         frm_Mensagem.btn_Falso.Visible := True;
         frm_Mensagem.ShowModal;
         frm_Mensagem.Caption := '';
         frm_Mensagem.lbl_Mensagem.Caption := '';
         frm_Mensagem.btn_Falso.Caption := '';
         frm_Mensagem.btn_Falso.Visible := False;
         FreeAndNil( frm_Mensagem );
         exit;
     End;
     if edt_TotalPedido.Text <> '' then
     begin
        TotalGeral    :=     strtofloat(edt_TotalPedido.Text);
     end;
    ValorUnitario :=     strtofloat(edt_ValorUnitario.Text);
    Quantidade    :=     strtofloat(edt_Quantidade.Text);
    ValorTotal    :=     ValorUnitario *  Quantidade;
    if Alteracao then
    begin
       cds_Pedidos.Edit;
    end
    else begin
       cds_Pedidos.Insert;
    end;
    cds_Pedidos.FieldByName('codProd').AsInteger   := CodProduto;
    cds_Pedidos.FieldByName('descricao').AsString     := DescProduto;
    cds_Pedidos.FieldByName('qtde').AsFloat           := Quantidade;
    cds_Pedidos.FieldByName('VlrUnit').AsFloat        := ValorUnitario;
    cds_Pedidos.FieldByName('VlrTotal').AsFloat       := ValorTotal;
    cds_Pedidos.Post;
    TotalGeral    :=     TotalGeral + ValorTotal;
    edt_TotalPedido.Text  := floattostr(TotalGeral) ;
    btn_GravarPedido.Enabled := true;
    cboProduto.Enabled := true;
    edt_Quantidade.Text := '';
    edt_ValorUnitario.Text := '' ;
    btn_AdicionarProduto.Caption := 'Adicionar Produto';
    cds_Pedidos.first;
end;

procedure Tfrm_LancamentoPedido.btn_BuscarPedidoClick(Sender: TObject);
begin
     btn_CancelarPedido.Enabled := False;
     if cds_Pedidos.Active = true then
     Begin
        cds_Pedidos.Close;
     End;
     cds_Pedidos.CreateDataSet;
     Application.CreateForm( Tfrm_BuscarPedido, frm_BuscarPedido );
     CodPedido := 0;
     frm_BuscarPedido.edt_NumPedido.Text := '';
     frm_BuscarPedido.ShowModal;
     FreeAndNil( frm_BuscarPedido );
     if CodPedido <> 0 then
     begin
        btn_CancelarPedido.Enabled := true;
        btn_CancelarPedido.Visible:= true;
        btn_GravarPedido.Enabled := False;
        PreenchePedido();
     end;
end;

procedure Tfrm_LancamentoPedido.btn_CancelarPedidoClick(Sender: TObject);
begin
     Application.CreateForm( tfrm_Mensagem, frm_Mensagem );
     frm_Mensagem.Caption := 'ATEN��O!';
     frm_Mensagem.lbl_Mensagem.Caption := 'Deseja Excluir o Pedido informado?';
     frm_Mensagem.btn_Verdadeiro.Caption := 'SIM';
     frm_Mensagem.btn_Verdadeiro.Visible := True;
     frm_Mensagem.btn_Falso.Caption := 'N�O';
     frm_Mensagem.btn_Falso.Visible := True;
     frm_Mensagem.RetornoMensagem  := False;
     frm_Mensagem.ShowModal;
     frm_Mensagem.Caption := '';
     frm_Mensagem.lbl_Mensagem.Caption := '';
     frm_Mensagem.btn_Verdadeiro.Caption := '';
     frm_Mensagem.btn_Verdadeiro.Visible := False;
     frm_Mensagem.btn_Falso.Caption := '';
     frm_Mensagem.btn_Falso.Visible := False;
     DeletaPedido :=  frm_Mensagem.RetornoMensagem;
     FreeAndNil( frm_Mensagem );
     if DeletaPedido then
     begin
       Dados.CancelaPedido(CodPedido);
     end;

     if Dados.Sucesso then
     begin
        Application.CreateForm( tfrm_Mensagem, frm_Mensagem );
        frm_Mensagem.Caption := 'SUCESSO!';
        frm_Mensagem.lbl_Mensagem.Caption := 'PEDIDO ' + inttostr(CodPedido) + ' APAGADO COM SUCESSO!';
        frm_Mensagem.btn_Verdadeiro.Caption := 'OK';
        frm_Mensagem.btn_Verdadeiro.Visible := True;
        frm_Mensagem.RetornoMensagem  := False;
        frm_Mensagem.ShowModal;
        frm_Mensagem.Caption := '';
        frm_Mensagem.lbl_Mensagem.Caption := '';
        frm_Mensagem.btn_Verdadeiro.Caption := '';
        frm_Mensagem.btn_Verdadeiro.Visible := False;
        FreeAndNil( frm_Mensagem );
     end;

end;


procedure Tfrm_LancamentoPedido.btn_GravarPedidoClick(Sender: TObject);
begin
  Dados.GravaPedido(CodCliente,strtofloat(edt_TotalPedido.text));
  cds_Pedidos.first;
  while not cds_Pedidos.Eof do begin
    Dados.GravaPedidoProduto(CodPedido,cds_Pedidos.FieldByName('CodProd').AsInteger,
                             cds_Pedidos.FieldByName('qtde').AsFloat,cds_Pedidos.FieldByName('VlrUnit').AsFloat,
                             cds_Pedidos.FieldByName('VlrTotal').AsFloat);
    cds_Pedidos.Next;
  end;
  cds_Pedidos.first;
     if Dados.Sucesso then
     begin
        Application.CreateForm( tfrm_Mensagem, frm_Mensagem );
        frm_Mensagem.Caption := 'SUCESSO!';
        frm_Mensagem.lbl_Mensagem.Caption := 'PEDIDO ' + inttostr(CodPedido) + ' GERADO COM SUCESSO!';
        frm_Mensagem.btn_Verdadeiro.Caption := 'OK';
        frm_Mensagem.btn_Verdadeiro.Visible := True;
        frm_Mensagem.RetornoMensagem  := False;
        frm_Mensagem.ShowModal;
        frm_Mensagem.Caption := '';
        frm_Mensagem.lbl_Mensagem.Caption := '';
        frm_Mensagem.btn_Verdadeiro.Caption := '';
        frm_Mensagem.btn_Verdadeiro.Visible := False;
        FreeAndNil( frm_Mensagem );
        LimpaDados;
        PreencheProduto;
        PreencheCliente;
     end;
end;

procedure Tfrm_LancamentoPedido.cboClienteChange(Sender: TObject);
begin
   if cboCliente.Text <> '' then
   begin
     edt_CodCliente.Text  := cboCliente.Text;
     CodCliente := strtoint(copy(cboCliente.Text,0,(pos(' -',cboCliente.Text)-1)));
     pnl_produtos.Enabled := true;
     btn_BuscarPedido.Enabled := false;
   end
   Else
   begin
     LimpaDados;
     PreencheCliente;
     pnl_produtos.Enabled := false;
     btn_BuscarPedido.Enabled := true;
     btn_GravarPedido.Enabled := True;
   end;

end;

procedure Tfrm_LancamentoPedido.cboClienteEnter(Sender: TObject);
begin
   LimpaDados;
   PreencheCliente;
   PreencheProduto;
end;

procedure Tfrm_LancamentoPedido.cboProdutoChange(Sender: TObject);
begin
   if  (Trim(cboProduto.Text) <> '') then
   begin
        CodProduto := strtoint(copy(cboProduto.Text,0,(pos(' -',cboProduto.Text)-1)));
        DescProduto := copy(cboProduto.Text,(pos('- ',cboProduto.Text)-1),LENGTH(cboProduto.Text));
        btn_AdicionarProduto.Enabled := true;
   end
   Else
   Begin
        btn_AdicionarProduto.Enabled := false;
        CodProduto := 0 ;
        DescProduto := '';
   end;
end;

procedure Tfrm_LancamentoPedido.dbg_PedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if (key = VK_DELETE) then
   begin
     if edt_TotalPedido.Text <> '' then
     begin
        edt_TotalPedido.Text := floattostr(strtofloat(edt_TotalPedido.Text) -
                                 cds_Pedidos.FieldByName('VlrTotal').AsFloat);

     end;
     cds_Pedidos.Delete;
   end
   ELSE
   If (key = VK_RETURN) then
   Begin
       btn_AdicionarProduto.Caption := 'Alterar Produto';
       edt_Quantidade.Text := floattostr(cds_Pedidos.FieldByName('qtde').AsFloat);
       edt_ValorUnitario.Text := floattostr(cds_Pedidos.FieldByName('VlrUnit').AsFloat);
       cboProduto.Items.IndexOf(InttoStr(cds_Pedidos.FieldByName('CodProd').AsInteger) + ' - ' +
                                          cds_Pedidos.FieldByName('descricao').AsString);
       CodProduto :=  cds_Pedidos.FieldByName('CodProd').AsInteger;
       edt_TotalPedido.Text := floattostr(strtofloat(edt_TotalPedido.Text) - cds_Pedidos.FieldByName('VlrTotal').AsFloat);
       cboProduto.Enabled := False;
       Alteracao := True;
   End;
end;

procedure Tfrm_LancamentoPedido.FormShow(Sender: TObject);
begin
    LimpaDados;
    PreencheProduto;
    PreencheCliente;

end;

procedure Tfrm_LancamentoPedido.PreenchePedido;
begin
   CodProduto := 0;
   Dados.SQLQUERY.Close();
   Dados.SQLQUERY.SQL.Clear;
   Dados.SQLQUERY.SQL.Add('select Ped.CodigoCliente,Clie.NomeCliente,PedIt.CodigoProduto, Pro.Descricao,');
   Dados.SQLQUERY.SQL.Add('Ped.ValorTotal,PedIt.ValorUnitario, PedIt.Quantidade, PedIt.ValorTotal, PedIt.CodigoPedidoItem ');
   Dados.SQLQUERY.SQL.add('from Pedidos Ped');
   Dados.SQLQUERY.SQL.add('Inner Join PedidosItem PedIt on PedIt.NumeroPedido = Ped.NumeroPedido');
   Dados.SQLQUERY.SQL.add('Inner Join Produtos Pro on PedIt.CodigoProduto = Pro.CodigoProduto');
   Dados.SQLQUERY.SQL.add('Inner Join Clientes Clie on Ped.CodigoCliente = Clie.CodigoCliente');
   Dados.SQLQUERY.SQL.add('Where Ped.NumeroPedido = ' + inttostr(CodPedido));
   Dados.SQLQUERY.SQL.add(' ORDER BY PedIt.CodigoPedidoItem');
   Dados.SQLQUERY.Open;
  If not Dados.SQLQUERY.Eof Then begin
     edt_CodCliente.Text := trim(inttostr(dados.SQLQUERY.Fields[0].AsInteger) + ' - ' + dados.SQLQUERY.Fields[1].AsString);
     cboCliente.Text :=  edt_CodCliente.Text;
     edt_TotalPedido.Text := floattostr(dados.SQLQUERY.Fields[4].AsFloat);
  end;
  while not Dados.SQLQUERY.Eof do begin
      cds_Pedidos.Insert;
      cds_Pedidos.Fields[0].AsInteger :=  (dados.SQLQUERY.Fields[2].AsInteger);
      cds_Pedidos.Fields[1].AsString :=  trim(dados.SQLQUERY.Fields[3].AsString);
      cds_Pedidos.Fields[2].AsFloat := (dados.SQLQUERY.Fields[5].AsFloat);
      cds_Pedidos.Fields[3].AsFloat := (dados.SQLQUERY.Fields[6].AsFloat);
      cds_Pedidos.Fields[4].AsFloat := (dados.SQLQUERY.Fields[7].AsFloat);
      cds_Pedidos.Post;
      Dados.SQLQUERY.Next;
  end;
   Dados.SQLQUERY.Close;
   cds_Pedidos.First;
end;


procedure Tfrm_LancamentoPedido.LimpaDados;
begin
   btn_CancelarPedido.Enabled := false;
   btn_GravarPedido.Enabled := false;
   btn_CancelarPedido.Visible := false;
   Alteracao := false;
   btn_BuscarPedido.Enabled := True;
   CodCliente := 0;
   CodProduto := 0;
   DescProduto := '';
   GravaPedido  := False;
   DeletaPedido := False;
   cboCliente.Items.Clear;
   cboProduto.Items.Clear;
   if cds_Pedidos.Active = true then
   Begin
      cds_Pedidos.Close;
   End;
   cds_Pedidos.CreateDataSet;
end;

end.
