unit BuscaPedido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, DBCONECTA,Mensagem;

type
  Tfrm_BuscarPedido = class(TForm)
    btn_retorna: TButton;
    btn_confirma: TButton;
    edt_NumPedido: TEdit;
    Panel1: TPanel;
    procedure edt_NumPedidoChange(Sender: TObject);
    procedure btn_confirmaClick(Sender: TObject);
    procedure btn_retornaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_BuscarPedido: Tfrm_BuscarPedido;

implementation
uses LancamentoPedido;

{$R *.dfm}

procedure Tfrm_BuscarPedido.btn_confirmaClick(Sender: TObject);

begin
  Dados.SQLQuery.Close;
  Dados.SQLQuery.SQL.clear;
  Dados.SQLQuery.SQL.add('select NumeroPedido from Pedidos where NumeroPedido = '+ edt_NumPedido.Text);
  Dados.SQLQuery.open;
  if Dados.SQLQuery.Eof then begin
     Dados.SQLQuery.Close;
     Application.CreateForm( tfrm_Mensagem, frm_Mensagem );
     frm_Mensagem.Caption := 'ERRO!';
     frm_Mensagem.lbl_Mensagem.Caption := 'Numero do Pedido n�o Encontrado';
     frm_Mensagem.btn_Falso.Caption := 'OK';
     frm_Mensagem.btn_Falso.Visible := True;
     frm_Mensagem.ShowModal;
     frm_Mensagem.Caption := '';
     frm_Mensagem.lbl_Mensagem.Caption := '';
     frm_Mensagem.btn_Falso.Caption := '';
     frm_Mensagem.btn_Falso.Visible := False;
     FreeAndNil( frm_Mensagem );
     Exit;
  end;
  LancamentoPedido.frm_LancamentoPedido.CodPedido := (Dados.SQLQuery.FieldByName('NumeroPedido').AsInteger);
  Dados.SQLQuery.Close;
  Dados.SQLQuery.SQL.Clear;
  Close;
end;

procedure Tfrm_BuscarPedido.btn_retornaClick(Sender: TObject);
begin
    edt_NumPedido.Text := '';
    LancamentoPedido.frm_LancamentoPedido.CodPedido := 0;
    Close;
end;

procedure Tfrm_BuscarPedido.edt_NumPedidoChange(Sender: TObject);
begin
   if edt_NumPedido.Text <> '' then
   begin
     btn_confirma.Enabled := True;
   end
   else
   begin
     btn_confirma.Enabled := False;
   end;
end;

end.
